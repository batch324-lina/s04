<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<title>S04:Access Modifiers and Encapsulation</title>
	</head>

	<body>
		<h1>Access Modifiers</h1>
		<p><?php //$building->name = 'Caswynn Building'; ?></p>
		<p><?php var_dump($building); ?></p>
		<p><?php echo $building->getName(); ?></p>
		<p><?php $building->setName('Caswynn Building'); ?></p>
		<p><?php echo $building->getName(); ?></p>

		<p><?php echo $building->getFloors(); ?></p>

		<p><?php var_dump($kopiko); ?></p>

		<p><?php echo $milk->getName(); ?></p>
		<?php $milk->setName('Bear Brand'); ?>
		<p><?php echo $milk->getName(); ?></p>

		<p><?php echo $condominium->getName(); ?></p>
		
	</body>
</html>
