<?php
require_once './code.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S04: activity</title>
</head>
<body>
    <h1>Building</h1>
    <p><?php echo $building->getName(); ?></p>
    <p><?php echo $building->getFloors(); ?></p>
    <p><?php echo $building->getAddress(); ?></p>
    <p><?php echo $building->setBuildingName('Caswynn Complex'); ?></p>
    <h1>Condominium</h1>
    <p><?php echo $condominium->getName(); ?></p>
    <p><?php echo $condominium->getFloors(); ?></p>
    <p><?php echo $condominium->getAddress(); ?></p>
    <p><?php echo $condominium->setCondoName('Enzo Tower'); ?></p>

    <h1>Check if the Modifiers are working</h1>
    <p><?php echo $building->name; ?></p>
</body>
</html>